to build pypi package :

- install build and twine

```sh
pip install build twine
```

```sh
python -m build --sdist --wheel
twine check dist/*
twine upload dist/*
```
